#ifndef DTW_H_
#define DTW_H_

#include <vector>
#include <deque>
#include <string>

class DTW
{

public:

    DTW();
    ~DTW();

    //! Loads the signals to be warped from a vector.
    /*!
      \param data A triple vector representing the input data. Input data is composed of any number of signals,
             that must all have the same number of dimensions. Each dimension of one signal must have the same number of
             points as the others, but different signals can have a different number of points. E.g. two signals
             S1 and S2, with 3 dimensions (x, y, z), and S1 with N points and S2 with M points. The inputs are arranged
             as data[signal_number][dimension_number][data_point].
      \return True if the loading was succesfull, false otherwise.
    */
    bool loadResources(const std::vector<std::vector<std::vector<double> > > &data);

    //! Loads the signals to be warped from files. Each signal must have its own file. Each column of a file represents
    //! a dimension of that signal. The data must be separated by white space.
    /*!
      \param filenames A vector containing the names of files to be loaded.
      \param columns Optional. If it has at least one element, only the columns of the file which have the same number
             as the entries in columns will be loaded. Column numbers start from 1. E.g if columns contains 2 elements,
             1 and 3, then only the first and third columns of the file will be loaded.
      \return True if the loading was succesfull, false otherwise.
    */
    bool loadResources(const std::vector<std::string> &filenames, std::vector<int> columns = std::vector<int>());

    //! Computes the dynamic time warping of the loaded signals.
    /*!
      \param windowSize The size of the window around the optimal path of the previous iteration. Default value is 4.
             Higher values are recommended for signals with high offsets or very different number of points.
             Lower values can be used when the signals are known to be fairly similar or for extra speed.
      \param hierarchicalStep Multiplicative factor of the number of points from one iteration of the hierarchical dynamic
             time warping to the next. Default values is 2.
      \param verbose If true the algorithm will detail its progress (current iteration, overal % done, eta...). Set to true by
             default and recommended for long runs.
    */
    void compute(const int &windowSize = 4, const int &hierarchicalStep = 2, const bool &verbose = true);

    //! Saves the warped signals to the hard disk.
    /*!
      \param filename Name template for the output file(s)
      \param option If equal to SAVE_TO_SEPARE_FILES (the default value), each signal will be saved to a separate file, using filename
             as the prefix, and adding a suffix _n where n is the number of the signal (in the order that they were loaded).
             If equal to SAVE_TO_ONE_FILE, each warped signal will be written to the file in the order they were loaded. In either case
             the signals are always written dimension by dimension in the order those were loaded, each dimension written as a column.
    */
    void saveToFile(std::string filename, int option = SAVE_TO_SEPARE_FILES) const;

    //! Prints out the loaded data. Usefull to check the data was loaded correctly.
    void displayData() const;

    //! Returns the optimal path obtained from the computation.
    /*!
      \return The optimal path.
    */
    std::deque<std::vector<int>> getPath() const;

    //! Returns the signals as loaded, but warped by the optimal path.
    //! Data returned is in the format [signal_number][dimension_number][data_point].
    /*!
      \return Signals warped by the optimal path.
    */
    std::vector<std::vector<std::vector<double>>> getWarpedData() const;

    //! Returns the last entry of the accumulated cost matrix (ACM), which gives a global measure of the similarity between the
    //! signals. The unit of that measure is the same unit as the output of the cost function use to fill the ACM.
    /*!
      \return Similarity measure.
    */
    double getSimilarityMeasure() const;

    enum
    {
        SAVE_TO_ONE_FILE,
        SAVE_TO_SEPARE_FILES
    };

    static std::vector<std::vector<double>> loadCompleteFile(const std::string &filename, int &signalDimensions);
    static std::vector<std::vector<double>> loadColumns(const std::string &filename, const std::vector<int> &columns);

private:

    /// Resource variables
    int m_numSignals;
    int m_numDims;

    double ***m_X;
    std::vector<int> m_sizeSignals;
    std::deque<std::vector<int>> m_path;
    bool m_valuesLoaded;
    bool m_isComputed;
    int *m_pow2;

    /// Compute variables
    int m_initCube;
    int m_slicePoints;
    int m_wd;
    int m_numSlices;
    double m_similarity;
    std::vector<int> m_Jtemp;
    std::vector<int> m_sideSum;
    std::vector<int> m_windowDirectionIndex;
    std::vector<std::vector<int>> m_Wcentral;
    std::vector<std::vector<int>> m_Wslice;
    std::vector<std::vector<int>> m_WsliceInverted;
    std::vector<std::vector<int>> m_Wdirections;

    void saveOneFile(std::string filePath) const;
    void saveSeparateFiles(std::string filePath) const;
    void clearData();

    void multidtw (double ***X, float *&DTW, std::deque<std::vector<int>> &w, const std::vector<int> &Sizes, const int signalsDim, double &similarity, const bool &verbose = true);
    void fastmultidtw (double ***X, std::deque<std::vector<int> > &w, const std::vector<int> &Sizes, int mult, const int signalsDim, double &similarity, const bool &verbose = true);
    void multicoarsedtw (double ***X, std::vector<std::vector<int> > &W, std::deque<std::vector<int>> &w,
                         const std::vector<int> &sizeDemo, const int &signalsDim, double &similarity, const bool &verbose = true);
    std::vector<std::vector<int>> removeDuplicatesSorted(const std::vector<std::vector<int>> &toCheck);
    void pathInterpolation(std::vector<std::vector<int> > &Wn, std::deque<std::vector<int> > &w, const std::vector<int> &sizeDemo, int mult);

    int nextpow (int p, int N);
    int coarsening(double *X, double *&Xc, int n, int size);

    void ind2sub(std::vector<int> &Id, const int &i, int &SliceNumber);
    void sub2ind(int &sub, const std::vector<int> &Id, const int &currentSlice);
    bool checkWindow(int &sub, const std::vector<int> &Id, const int &currentSlice);

    inline void ind2sub_basic(int *Id, const int NBSAMPLES, int *Sum, int i)
    {
        int N=i;
        for (int j=0; j<NBSAMPLES; j++)
        {
            Id[j] = (int)((N)/Sum[j]);
            N = N - Id[j]*Sum[j];
        }
    }

    inline int sub2ind_basic(int *Id, const int NBSAMPLES, int *Sum)
    {
        int N=0;
        for (int j=0; j<NBSAMPLES; j++)
            N = N + Id[j]*Sum[j];
        return N;
    }

    inline int pown(int n, int p)
    {
        int N=1;
        for (int i=0; i<p; i++)
            N=N*n;
        return N;
    }

};

#endif
