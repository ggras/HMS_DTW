# Hierarchical Dynamic Time Warping

This project contains an efficient dynamic time warping algorithm for the alignment of 2 or more multi-dimensional signals. Windowing is preformed hierarchically on sucessive undersampled versions of the initial signal. Additional details can be found in the corresponding publication at https://ieeexplore.ieee.org/abstract/document/6906633/.

### Prerequisites

A compiler with C++11 support (MSVC 14.0 or higher on Windows).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

