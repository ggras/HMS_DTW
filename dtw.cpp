#include "dtw.h"
#include <sstream>
#include <chrono>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <limits>
#include <stdlib.h>

using namespace std;

DTW::DTW()
    : m_X(NULL)
    , m_valuesLoaded(false)
    , m_isComputed(false)
{
    m_pow2 = new int[30];
    m_pow2[0] = 1;
    for (int i = 1; i < 30; i++)
        m_pow2[i] = m_pow2[i-1]*2;
}

DTW::~DTW()
{
    clearData();
    delete [] m_pow2;
}

void DTW::clearData()
{
    if (m_X != NULL)
    {
        for (int i = 0; i < m_numSignals; i++)
        {
            for (int j = 0; j < m_numDims; j++)
                delete [] m_X[i][j];
            delete [] m_X[i];
        }
    }
    delete [] m_X;
}

bool DTW::loadResources(const std::vector<std::vector<std::vector<double>>> &data)
{
    unsigned numDemos = data.size();
    if (numDemos < 2)
    {
        cerr << "\nDTW: Too few signals provided during resource loading." << endl;
        return false;
    }
    unsigned numDims  = data[0].size();
    bool checkDims = true;
    for (unsigned i = 1; i < data.size(); i++)
    {
        if (data[i].size() != data[i-1].size())
        {
            checkDims = false;
            break;
        }
    }
    if (checkDims == false)
    {
        cerr << "\nDTW: Signals do not all have the same dimensions" << endl;
        return false;
    }
    if (numDims < 1)
    {
        cerr << "\nDTW: Signals need at least one dimension." << endl;
        return false;
    }

    bool checkSamples = true;
    unsigned badIndex;
    for (unsigned i = 0; i < data.size(); i++)
    {
        for (unsigned j = 1; j < data[i].size(); j++)
        {
            if (data[i][j].size() != data[i][j-1].size())
            {
                checkSamples = false;
                badIndex = i;
                break;
            }
        }
        if (!checkSamples)
            break;
    }
    if (checkSamples == false)
    {
        cerr << "\nDTW: Signal number " << badIndex+1 << " (counting from 1) has dimensions with different numbers of samples" << endl;
        return false;
    }

    clearData();
    m_isComputed    = false;
    m_numSignals  = numDemos;
    m_numDims     = numDims;
    m_sizeSignals = std::vector<int>(m_numSignals);

    m_X = new double **[m_numSignals];
    for (int i = 0; i < m_numSignals; i++)
        m_X[i] = new double *[m_numDims];
    for (int i = 0; i < m_numSignals; i++)
    {
        m_sizeSignals[i] = data[i][0].size();
        for (int j = 0; j < m_numDims; j++)
        {
            m_X[i][j] = new double[m_sizeSignals[i]];
            for (int k = 0; k < m_sizeSignals[i]; k++)
                m_X[i][j][k] = data[i][j][k];
        }
    }
    m_valuesLoaded = true;
    return true;
}

bool DTW::loadResources(const std::vector<std::string> &filenames, std::vector<int> columns)
{
    if (filenames.size() < 2)
    {
        cerr << "\nDTW: Too few signals provided during resource loading." << endl;
        return false;
    }

    std::vector<std::vector<std::vector<double>>> data(filenames.size());
    for (unsigned i = 0; i < filenames.size(); i++)
    {
        if (columns.size() == 0)
            data[i] = loadCompleteFile(filenames[i], m_numDims);
        else
        {
            data[i] = loadColumns(filenames[i], columns);
            m_numDims = columns.size();
        }
    }
    return loadResources(data);
}

std::vector<std::vector<double>> DTW::loadCompleteFile(const std::string &filename, int &signalDimensions)
{
    ifstream file(filename);
    unsigned n = 0;
    signalDimensions = 0;
    string line;
    if (getline(file, line))
    {
        n++;
        std::stringstream ss(line);
        double tc;
        while (ss >> tc)
            signalDimensions++;
    }
    while (getline(file, line))
        ++n;
    file.clear();
    file.seekg(0, ios::beg);
    if (n == 0)
    {
        cerr << "\nFile " << filename << " is empty." << endl;
        return std::vector<std::vector<double>>();
    }

    std::vector<std::vector<double>> out(signalDimensions, std::vector<double>(n));
    for (unsigned j = 0; j < n; j++)
    {
        for (int i = 0; i < signalDimensions; i++)
            file >> out[i][j];
    }
    file.close();
    return out;
}

std::vector<std::vector<double>> DTW::loadColumns(const std::string &filename, const std::vector<int> &columns)
{
    if (columns.size() == 0)
    {
        return std::vector<std::vector<double>>();
    }

    ifstream file(filename);
    unsigned n = 0;
    string line;
    while (getline(file, line))
        ++n;
    file.clear();
    file.seekg(0, ios::beg);
    if (n == 0)
    {
        cerr << "\nFile " << filename << " is empty." << endl;
        return std::vector<std::vector<double>>();
    }

    std::vector<std::vector<double>> out(columns.size(), std::vector<double>(n));
    for (unsigned j = 0; j < n; j++)
    {
        double temp;
        int column = 1;
        for (unsigned i = 0; i < columns.size(); i+=0)
        {
            file >> temp;
            if (columns[i] == column)
            {
                out[i][j] = temp;
                i++;
            }
            column++;
        }
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    file.close();
    return out;
}

void DTW::displayData() const
{
    if (!m_valuesLoaded)
    {
        cerr << "\nERROR: values not loaded" << endl;
        return;
    }

    for (int i = 0; i < m_numSignals; i++)
    {
        for (int k = 0; k < m_sizeSignals[i]; k++)
        {
            for (int j = 0; j < m_numDims; j++)
                cout << m_X[i][j][k] << " ";
            cout << endl;
        }
        cout << "=======================================================================" << endl;
        cout << "=======================================================================" << endl;
        cout << "=======================================================================" << endl;
    }
}

std::deque<std::vector<int>> DTW::getPath() const
{
    return m_path;
}

std::vector<std::vector<std::vector<double>>> DTW::getWarpedData() const
{
    if (!m_isComputed)
    {
        cerr << "\nDTW: computation not done, cannot return result" << endl;
        return std::vector<std::vector<std::vector<double>>>();
    }

    std::vector<std::vector<std::vector<double>>> data(m_numSignals, std::vector<std::vector<double>>(m_numDims, std::vector<double>(m_path.size())));
    for (int i = 0; i < m_numSignals; i++)
    {
        for (int j = 0; j < m_numDims; j++)
        {
            for (unsigned k = 0; k < m_path.size(); k++)
                data[i][j][k] = m_X[i][j][m_path[k][i]];
        }
    }
    return data;
}

double DTW::getSimilarityMeasure() const
{
        if (!m_isComputed)
        {
            cerr << "\nDTW: computation not done, cannot return result" << endl;
            return -1;
        }
        return m_similarity;
}

void DTW::compute(const int &windowSize, const int &hierarchicalStep, const bool &verbose)
{
    if (!m_valuesLoaded)
    {
        cerr << "\nDTW: values not loaded" << endl;
        return;
    }
    m_wd = windowSize;
    fastmultidtw (m_X, m_path, m_sizeSignals, hierarchicalStep, m_numDims, m_similarity, verbose);
    m_isComputed = true;
}

void DTW::saveToFile(string filePath, int option) const
{
    if (option == SAVE_TO_ONE_FILE)
        saveOneFile(filePath);
    else if (option == SAVE_TO_SEPARE_FILES)
        saveSeparateFiles(filePath);
}

void DTW::saveOneFile(string filePath) const
{
    ofstream result(filePath);
    if (!result)
    {
        cerr << "\nCould not open file: " << filePath << endl;
        return;
    }

    for (unsigned k = 0; k < m_path.size(); k++)
    {
        for (int i = 0; i < m_numSignals; i++)
        {
            for (int j = 0; j < m_numDims; j++)
                result << m_X[i][j][m_path[k][i]] << " ";
        }
        result << endl;
    }
    result.close();
}

void DTW::saveSeparateFiles(string filePath) const
{
    for (int i = 0; i < m_numSignals; i++)
    {
        stringstream ss;
        ss << filePath << string("_") << i+1;
        string filename = ss.str();
        ofstream result(filename);
        if (!result)
        {
            cerr << "\nCould not open file: " << filename << endl;
            return;
        }

        for (unsigned k = 0; k < m_path.size(); k++)
        {
            for (int j = 0; j < m_numDims; j++)
                result << m_X[i][j][m_path[k][i]] << " ";
            result << endl;
        }
        result.close();
    }
}

// STATIC DYNAMIC TIME WARPING FUNCTIONS ===============================================================================================
// =====================================================================================================================================

void DTW::multidtw (double ***X, float *&D, std::deque<std::vector<int> > &w, const std::vector<int> &sizeDemo, const int signalsDim, double &similarity, const bool &verbose)
{
    int lenD = 1;
    for (int i = 0; i < m_numSignals; i++)
        lenD = lenD*sizeDemo[i];
    D = new float[lenD];
    if (verbose)
        cout << "\nAccumulated cost matrix size: " << (double)lenD*sizeof(float)/1048576 << " MB" << endl;
    D[0]=0;
    for (int i=1; i < lenD; i++)
        D[i] = std::numeric_limits<float>::max();

    int *Sum = new int [m_numSignals];
    for (int i=0; i<m_numSignals; i++)
    {
        Sum[i] = 1;
        for (int j=i+1; j<m_numSignals; j++)
            Sum[i] = Sum[i]*sizeDemo[j];
    }

    int *Id = new int[m_numSignals];
    int *Jd = new int[m_numSignals];
    int *Sd = new int[m_numSignals];
    int *permutablesIndex = new int[m_numSignals];
    int *permuteArray = new int[m_numSignals];
    int dimPermute;
    double cost;
    double mini;
    double temp = 0;

    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
    std::chrono::duration<double, std::milli> t;
    for (int i=1; i < lenD; i++)
    {
        cost = 0;
        dimPermute = -1;
        int N=i;
        for (int j=0; j<m_numSignals; j++)
            permutablesIndex[j] = 0;

        for (int j=0; j<m_numSignals; j++)
        {
            Id[j] = (int)((N)/Sum[j]);
            N = N - Id[j]*Sum[j];
            if (Id[j]!=0)
            {
                dimPermute++;
                permutablesIndex[dimPermute] = j;
            }
        }

        for (int j = 0; j < m_numSignals; j++)
        {
            for (int k = j+1; k < m_numSignals; k++)
            {
                double buff = 0;
                for (int p = 0; p < signalsDim; p++)
                    buff  = buff + pow(X[j][p][Id[j]] - X[k][p][Id[k]],2);
                cost = cost + sqrt(buff);
            }
        }

        mini = std::numeric_limits<float>::max();
        for (int j=0; j<dimPermute; j++)
            permuteArray[j] = 0;

        for (int j = 1; j <= m_pow2[dimPermute+1]-1; j++)
        {
            for (int k = 0; k < m_numSignals; k++)
                Jd[k] = Id[k];

            for (int k = 0; k <= dimPermute; k++)
            {
                int kt = dimPermute - k;
                permuteArray[kt] = (int)(j/m_pow2[k])%2;
                Jd[permutablesIndex[kt]] = Jd[permutablesIndex[kt]] - permuteArray[kt];
            }

            int tempInd = sub2ind_basic(Jd, m_numSignals, Sum);
            temp = D[tempInd];
            if (temp < mini)
                mini = temp;
        }

        D[i] = (float)(cost + mini);

        if (verbose)
        {
            t = std::chrono::system_clock::now() - start;
            if (t.count() > 1000)
            {
                cout << "Progress: " << (double)i/(double)lenD*100 << "%" << endl;
                start = std::chrono::system_clock::now();
            }
        }
    }
    similarity = D[lenD - 1];

    if (verbose)
        cout << "Done with init acc matrix, retracing path..." << endl;

    std::vector<int> wtemp(m_numSignals);
    for (int i=0; i<m_numSignals; i++)
    {
        Id[i] = sizeDemo[i]-1;
        wtemp[i] = sizeDemo[i] - 1;
    }

    w.clear();
    w.push_front(wtemp);

    for (int i = 0; i < m_numSignals; i++)
    {
        while (Id[i] != 0)
        {
            dimPermute = -1;
            for (int j=0; j<m_numSignals; j++)
                permutablesIndex[j] = 0;

            for (int j=0; j<m_numSignals; j++)
            {
                if (Id[j]!=0)
                {
                    dimPermute++;
                    permutablesIndex[dimPermute] = j;
                }
            }

            mini = std::numeric_limits<float>::max();
            for (int j=0; j<dimPermute; j++)
                permuteArray[j] = 0;

            for (int j = 1; j <= m_pow2[dimPermute+1]-1; j++)
            {
                for (int k = 0; k < m_numSignals; k++)
                    Jd[k] = Id[k];

                for (int k = 0; k <= dimPermute; k++)
                {
                    int kt = dimPermute - k;
                    permuteArray[kt] = (int)(j/m_pow2[k])%2;
                    Jd[permutablesIndex[kt]] = Jd[permutablesIndex[kt]] - permuteArray[kt];
                }

                int tempInd = sub2ind_basic(Jd, m_numSignals, Sum);
                temp = D[tempInd];
                if (temp < mini)
                {
                    mini = temp;
                    for (int k = 0; k < m_numSignals; k++)
                        Sd[k] = Jd[k];
                }
            }

            for (int j = 0; j < m_numSignals; j++)
            {
                Id[j] = Sd[j];
                wtemp[j] = Id[j];
            }
            w.push_front(wtemp);
        }
    }

    delete [] Sum;
    delete [] Id;
    delete [] Jd;
    delete [] Sd;
    delete [] permutablesIndex;
    delete [] permuteArray;
}

void DTW::fastmultidtw (double ***X, std::deque<std::vector<int>> &w, const std::vector<int> &sizeDemo1, int mult, const int signalsDim, double &similarity, const bool &verbose)
{
    if (verbose)
    {
        cout << "\nUndersampling coefficient: " << mult;
        cout << "\nWindow size: " << (int)m_wd << endl;
    }
    int T=0;
    for (int i = 0; i < m_numSignals; i++)
        T=T+sizeDemo1[i];
    double avgS = T/m_numSignals;
    int num = nextpow((int)(avgS/10),mult);
    int v0 = pown(mult, num);
    int v=v0;
    std::vector<int> sizeDemo(m_numSignals);
    double ***Xc = new double**[m_numSignals];
    for (int i = 0; i < m_numSignals; i++)
    {
        Xc[i] = new double*[signalsDim];
        for (int j = 0; j < signalsDim; j++)
            sizeDemo[i] = coarsening(X[i][j],Xc[i][j],v,sizeDemo1[i]);
    }

    float *D;
    if (verbose)
        cout << "Initial undersampled warping..." << endl;
    multidtw(Xc,D,w,sizeDemo, signalsDim, similarity, verbose);

    for (int i = 0; i < m_numSignals; i++)
    {
        for (int j = 0; j < signalsDim; j++)
            delete [] Xc[i][j];
        delete [] Xc[i];
    }
    delete [] Xc;
    delete [] D;

    while (v!=1)
    {
        v=v/mult;
        if (verbose)
            cout << "\nIteration number " << num-- << endl;
        double ***Xc = new double**[m_numSignals];
        int avgS = 0;
        for (int i = 0; i < m_numSignals; i++)
        {
            Xc[i] = new double*[signalsDim];
            for (int j = 0; j < signalsDim; j++)
                sizeDemo[i] = coarsening(X[i][j],Xc[i][j],v,sizeDemo1[i]);
            avgS = avgS + sizeDemo[i];
        }
        avgS = avgS/m_numSignals;

        std::vector<std::vector<int>> Wn;
        pathInterpolation(Wn, w, sizeDemo, mult);
        multicoarsedtw(Xc, Wn, w, sizeDemo, signalsDim, similarity, verbose);

        for (int i = 0; i < m_numSignals; i++)
        {
            for (int j = 0; j < signalsDim; j++)
                delete [] Xc[i][j];
            delete [] Xc[i];
        }
        delete [] Xc;
    }
}

void DTW::pathInterpolation(std::vector<std::vector<int>> &Wn, std::deque<std::vector<int>> &w, const std::vector<int> &sizeDemo, int mult)
{
    int dim = sizeDemo.size();
    int S = w.size();
    int counterNW = 0;
    std::vector<int> Wtemp(dim, 0);
    std::vector<int> maxDistance(dim, 0);
    Wn.push_back(Wtemp);

    for (int i = 1; i < S; i++)
    {
        int totaljumps = 0;
        for (int j = 0; j < dim; j++)
        {
            /// mult*(w[i][j] + 1) - 1 is to account properly for the multiplication factor
            maxDistance[j] = std::min(sizeDemo[j] - 1, mult*(w[i][j] + 1) - 1 - Wn[counterNW][j]);
            totaljumps += maxDistance[j];
        }

        std::vector<int> currDistance(dim, 0);
        while(currDistance != maxDistance)
        {
            for (int j = 0; j < dim; j++)
            {
                if (currDistance[j] < maxDistance[j])
                {
                    Wtemp = Wn[counterNW];
                    Wtemp[j]++;
                    counterNW++;
                    currDistance[j]++;
                    Wn.push_back(Wtemp);
                }
            }
        }
    }

    bool isequal = true;
    for (int j = 0; j < dim; j++)
        isequal = isequal && (mult*Wn[counterNW][j] == sizeDemo[j]-1);
    if (!isequal)
    {
        int totaljumps = 0;
        for (int j = 0; j < dim; j++)
        {
            maxDistance[j] = sizeDemo[j] - 1 - Wn[counterNW][j];
            totaljumps += maxDistance[j];
        }

        std::vector<int> currDistance(dim, 0);
        while(currDistance != maxDistance)
        {
            for (int j = 0; j < dim; j++)
            {
                if (currDistance[j] < maxDistance[j])
                {
                    Wtemp = Wn[counterNW];
                    Wtemp[j]++;
                    counterNW++;
                    currDistance[j]++;
                    Wn.push_back(Wtemp);
                }
            }
        }
    }
}

void DTW::multicoarsedtw (double ***X, std::vector<std::vector<int>> &W, std::deque<std::vector<int>> &w,
                          const std::vector<int> &sizeDemo, const int &signalsDim, double &similarity, const bool &verbose)
{
    /// Generate initial windowing parameters.
    int minSize = std::numeric_limits<int>::max();
    for (int i = 0; i < m_numSignals; i++)
    {
        if (minSize < sizeDemo[i])
            minSize = sizeDemo[i];
    }
    if (2*m_wd + 1 > minSize)
        m_wd = (int)((minSize - 1)/2);
    int cubeSide = 2*m_wd + 1;
    int S = W.size();

    /// Find the first index after which there is movement.
    int Istart = 0;
    for (int i = 1; i < S; i++)
    {
        for (int j = 0; j < m_numSignals; j++)
        {
            if (W[i][j] > m_wd)
            {
                Istart = i - 1;
                break;
            }
        }
        if (Istart != 0)
            break;
    }
    int Iend = S - 1;

    /// Adjust Wslice so that the full window will always appear, and remove duplicates.
    m_initCube = pown(cubeSide, m_numSignals) - 1;
    m_slicePoints = pown(cubeSide, m_numSignals-1);
    m_Wslice = std::vector<std::vector<int>>(Iend - Istart, std::vector<int>(m_numSignals, 0));
    for (int i = Istart + 1; i <= Iend; i++)
    {
        for (int j = 0; j < m_numSignals; j++)
        {
            if (W[i][j] - m_wd < 0)
                m_Wslice[i - Istart - 1][j] = m_wd;
            else if (W[i][j] + m_wd > sizeDemo[j] - 1)
                m_Wslice[i - Istart - 1][j] = sizeDemo[j] - 1 - m_wd;
            else
                m_Wslice[i - Istart - 1][j] = W[i][j];
        }
    }
    m_Wslice = removeDuplicatesSorted(m_Wslice);

    /// Invert Wslice for cache coherency in slice number search
    m_WsliceInverted = std::vector<std::vector<int>>(m_numSignals, std::vector<int>(m_Wslice.size(), 0));
    for (int i = 0; i < m_numSignals; i++)
    {
        for (unsigned j = 0; j < m_Wslice.size(); j++)
        {
            m_WsliceInverted[i][j] = m_Wslice[j][i];
        }
    }

    /// Find total length and directions.
    m_numSlices = m_Wslice.size();
    int SlicesTotal = m_numSlices*m_slicePoints;
    int lenD = SlicesTotal + m_initCube + 1;
    std::vector<int> WInitSlice(m_numSignals, m_wd);
    m_Wdirections = std::vector<std::vector<int>>(m_numSlices, std::vector<int>(m_numSignals, 0));

    for (int j = 0; j < m_numSignals; j++)
        m_Wdirections[0][j] = m_Wslice[0][j] - WInitSlice[j];
    for (int i = 1; i < m_numSlices; i++)
    {
        for (int j = 0; j < m_numSignals; j++)
            m_Wdirections[i][j] = m_Wslice[i][j] - m_Wslice[i-1][j];
    }

    /// Find the coordinates of slice centers relative to the center of the cube.
    m_Wcentral = std::vector<std::vector<int>> (m_numSlices, std::vector<int>(m_numSignals, 0));
    m_windowDirectionIndex = std::vector<int>(m_numSlices);
    for (int i = 0; i < m_numSlices; i++)
    {
        for (int j = 0; j < m_numSignals; j++)
        {
            m_Wcentral[i][j] = m_Wslice[i][j] + m_wd*m_Wdirections[i][j];
            if (m_Wdirections[i][j] == 1)
                m_windowDirectionIndex[i] = j;
        }
    }

    /// Precompute sums.
    m_sideSum = std::vector<int>(m_numSignals, 1);
    for (int i = 0; i < m_numSignals; i++)
    {
        for (int j = i+1; j < m_numSignals; j++)
            m_sideSum[i] *= cubeSide;
    }

    float * D = new float[lenD];
    D[0]=0;
    for (int i=1; i < lenD; i++)
        D[i] = std::numeric_limits<float>::max();
    if (verbose)
        cout << "\nAccumulated cost matrix size: " << (double)lenD*sizeof(float)/1048576 << " MB" << endl;
    double cost;
    float mini;
    int currentSlice = 0;
    std::vector<int> Id(m_numSignals);
    std::vector<int> Jd(m_numSignals);
    std::vector<int> Sd(m_numSignals);
    m_Jtemp = std::vector<int>(m_numSignals);
    double avgSpeed = 0;
    double iOld = 0;
    int iterations = 0;

    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
    std::chrono::duration<double, std::milli> t;
    for (int i = 1; i < lenD; i++)
    {
        ind2sub(Id, i, currentSlice);

        /// Compute the cost.
        cost = 0;
        for (int j = 0; j < m_numSignals; j++)
        {
            for (int k = j+1; k < m_numSignals; k++)
            {
                double buff = 0;
                for (int p = 0; p < signalsDim; p++)
                    buff  = buff + pow(X[j][p][Id[j]] - X[k][p][Id[k]],2);
                cost = cost + sqrt(buff);
            }
        }

        /// Iterate over all permutations to find the minimum.
        mini = std::numeric_limits<float>::max();
        for (int j = 1; j <= m_pow2[m_numSignals] - 1; j++)
        {
            for (int k = 0; k < m_numSignals; k++)
                Jd[k] = Id[k];
            bool valid = true;

            for (int k = 0; k < m_numSignals; k++)
            {
                int kt = m_numSignals - k - 1;
                int permuteArray = (int)(j/m_pow2[k])%2;
                Jd[kt] = Jd[kt] - permuteArray;

                /// Check if new index fits within signal boundaries.
                if (Jd[kt] < 0 || Jd[kt] > sizeDemo[kt] - 1)
                {
                    valid = false;
                    break;
                }
            }

            /// Check subscript is inside window and compute mini.
            int sub;
            if (valid)
                valid = checkWindow(sub, Jd, currentSlice);
            if (valid == true && D[sub] < mini)
                mini = D[sub];
        }
        D[i] = (float)cost + mini;

        if (verbose)
        {
            t = std::chrono::system_clock::now() - start;
            if (t.count() > 1000)
            {
                string eta("nan");
                if (i > 0)
                {
                    double Tnow = t.count()/1000;
                    double speed = Tnow/((double)i - (double)iOld);
                    iOld = i;
                    avgSpeed = (avgSpeed*iterations + speed)/(iterations+1);
                    iterations++;

                    int remaining = lenD - i;
                    int seconds = (int)avgSpeed*remaining;
                    int minutes = (seconds - seconds%60)/60;
                    seconds = seconds - minutes*60;
                    int hours = (minutes - minutes%60)/60;
                    minutes = minutes - hours*60;
                    stringstream ss;
                    ss << hours << "h " << minutes << "m " << seconds << "s ";
                    eta = ss.str();
                }

                cout << fixed << setprecision(4) << "Progress: " << setfill(' ') << setw(7) << (double)i/(double)lenD*100 << "%. ETA: " << eta << endl;
                start = std::chrono::system_clock::now();
            }
        }
    }
    similarity = D[lenD - 1];

    if (verbose)
        cout << "Done with init acc matrix, retracing path..." << endl;

    std::vector<int> wtemp(m_numSignals);
    for (int i = 0; i < m_numSignals; i++)
    {
        Id[i] = sizeDemo[i]-1;
        wtemp[i] = sizeDemo[i] - 1;
    }
    w.clear();
    w.push_front(wtemp);
    currentSlice = m_numSlices - 1;

    for (int i = 0; i < m_numSignals; i++)
    {
        while (Id[i] != 0)
        {
            /// Get current slice index
            int sliceLookBack = 1 + (m_numSignals - 1) * (2 * m_wd + 1);
            int SliceIndex = std::max(0, currentSlice - sliceLookBack);
            for (int j = 0; j < m_numSignals; j++)
            {
                for (int k = SliceIndex; k < m_numSlices; k++)
                {
                    if (m_WsliceInverted[j][k] + m_wd >= Id[j])
                    {
                        SliceIndex = k;
                        break;
                    }
                }
            }
            currentSlice = SliceIndex;

            mini = std::numeric_limits<float>::max();
            for (int j = 1; j <= m_pow2[m_numSignals] - 1; j++)
            {
                for (int k = 0; k < m_numSignals; k++)
                    Jd[k] = Id[k];
                bool valid = true;

                for (int k = 0; k < m_numSignals; k++)
                {
                    int kt = m_numSignals - k - 1;
                    int permuteArray = (int)(j/m_pow2[k])%2;
                    Jd[kt] = Jd[kt] - permuteArray;

                    if (Jd[kt] < 0 || Jd[kt] > sizeDemo[kt] - 1)
                    {
                        valid = false;
                        break;
                    }
                }

                /// Check subscript is inside window and compute mini.
                int sub;
                if (valid)
                    valid = checkWindow(sub, Jd, currentSlice);
                if (valid == true && D[sub] < mini)
                {
                    mini = D[sub];
                    for (int k = 0; k < m_numSignals; k++)
                        Sd[k] = Jd[k];
                }
            }

            for (int j = 0; j < m_numSignals; j++)
            {
                Id[j] = Sd[j];
                wtemp[j] = Id[j];
            }
            w.push_front(wtemp);
        }
    }

    delete [] D;
}

void DTW::ind2sub(std::vector<int> &Id, const int &i, int &SliceNumber)
{
    if (i <= m_initCube)
    {
        /// Check each dimension starting from the deepest (last dimension is the fastest changing).
        int N = i;
        for (int j = 0; j < m_numSignals; j++)
        {
            Id[j] = (int)((N)/m_sideSum[j]);
            N = N - Id[j]*m_sideSum[j];
        }
    }
    else
    {
        /// Find corresponding path slice (Wslice and Wdirections)
        /// -1 because indexes start from 0.
        int N = i - m_initCube - 1;
        SliceNumber = (int)(N/m_slicePoints);

        /// Iterate over each dimension. sideSumCounter is needed to not be affected
        /// by the jump in j.
        N = N - SliceNumber * m_slicePoints;
        int sideSumCounter = 1;
        for (int j = 0; j < m_numSignals; j++)
        {
            if (j != m_windowDirectionIndex[SliceNumber])
            {
                Id[j] = (int)(N/m_sideSum[sideSumCounter]);
                N = N - Id[j]*m_sideSum[sideSumCounter];
                Id[j] += m_Wcentral[SliceNumber][j] - m_wd;
                sideSumCounter++;
            }
        }
        Id[m_windowDirectionIndex[SliceNumber]] = m_Wcentral[SliceNumber][m_windowDirectionIndex[SliceNumber]];
    }
}

void DTW::sub2ind(int &sub, const std::vector<int> &Id, const int &currentSlice)
{
    bool insideCube = true;
    for (int j = 0; j < m_numSignals; j++)
    {
        if (Id[j] > 2*m_wd)
            insideCube = false;
    }

    if (insideCube)
    {
        sub = 0;
        for (int j = 0; j < m_numSignals; j++)
            sub = sub + m_sideSum[j] * (Id[j]);
    }
    else
    {
        /// Find which slice we are in.
        /// sliceLookBack represents how far back a slice can still have an impact
        int sliceLookBack = 1 + (m_numSignals - 1) * (2 * m_wd + 1);
        int SliceIndex = std::max(0, currentSlice - sliceLookBack);
        for (int j = 0; j < m_numSignals; j++)
        {
            for (int k = SliceIndex; k < m_numSlices; k++)
            {
                if (m_WsliceInverted[j][k] + m_wd >= Id[j])
                {
                    SliceIndex = k;
                    break;
                }
            }
        }

        /// Start from 0 and add the length of the cube and previous slices.
        /// Then iterate over each dimension, skipping the direction dimension.
        sub = m_initCube + m_slicePoints * SliceIndex + 1;
        int sliceCounter = 1;
        int Woffset;
        for (int j = 0; j < m_numSignals; j++)
        {
            if (j != m_windowDirectionIndex[SliceIndex])
            {
                Woffset = Id[j] - m_Wcentral[SliceIndex][j] + m_wd*(1 - m_Wdirections[SliceIndex][j]);
                sub += m_sideSum[sliceCounter] * Woffset;
                sliceCounter++;
            }
        }
    }
}

bool DTW::checkWindow(int &sub, const std::vector<int> &Id, const int &currentSlice)
{
    int tempSlice;
    sub2ind(sub, Id, currentSlice);
    ind2sub(m_Jtemp, sub, tempSlice);

    bool checkEquals = true;
    for (int i = 0;  i < m_numSignals; i++)
        checkEquals = checkEquals && (Id[i] == m_Jtemp[i]);
    return checkEquals;
}

std::vector<std::vector<int>> DTW::removeDuplicatesSorted(const std::vector<std::vector<int>> &toCheck)
{
    std::vector<std::vector<int>> unique;
    if (toCheck.size() == 0)
        return unique;

    int counter = 0;
    unique.push_back(toCheck[0]);
    for (unsigned i = 1; i < toCheck.size(); i++)
    {
        if (toCheck[i] != unique[counter])
        {
            unique.push_back(toCheck[i]);
            counter++;
        }
    }
    return unique;
}

int DTW::nextpow (int p, int N)
{
    int n=0;
    while (pown(N, n) < p)
        n=n+1;
    return n;
}

int DTW::coarsening(double *X, double *&Xc, int n, int size)
{
    int N = max(1, (int)(size/n));
    Xc = new double[N];
    for (int i = 0; i < N; i++)
        Xc[i] = X[n*(i+1)-1];
    return N;
}
