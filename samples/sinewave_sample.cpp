#include <iostream>
#include <dtw.h>
#include <fstream>
#include <cmath>

#define PI 3.14159265358979323846264338327
using namespace std;

std::vector<double> generateSineWave(const unsigned &numPoints)
{
    std::vector<double> sine(numPoints);
    for (unsigned i = 0; i < numPoints; i++)
    {
        sine[i] = std::sin(2.0 * PI * (double)i/(double)(numPoints-1));
    }
    return sine;
}

int main()
{
        std::vector<std::vector<std::vector<double> > > data;
        /// First vector contains the samples for a single dimension of a single signal
        std::vector<double> x1 = generateSineWave(100);
        std::vector<double> x2 = generateSineWave(150);
        /// Second vector contains the samples for all the dimensions of a single signal
        std::vector<std::vector<double>> d1; d1.push_back(x1);
        std::vector<std::vector<double>> d2; d2.push_back(x2);
        /// Final vector contains all signals
        data.push_back(d1);
        data.push_back(d2);

        DTW dtw;
        dtw.loadResources(data);
        dtw.compute(6, 2, false);
        std::cout << "Similarity measure: " << dtw.getSimilarityMeasure() << std::endl;

        /// Output results are stored in the same way as the inputs
        std::vector<std::vector<std::vector<double>>> result = dtw.getWarpedData();
        std::vector<double> & x1_warped = result[0][0];
        std::vector<double> & x2_warped = result[1][0];
        std::ofstream outFile("results.txt");
        for (unsigned i = 0; i < x1_warped.size(); i++)
        {
            outFile << x1_warped[i] << " " << x2_warped[i] << "\n";
        }
        outFile << std::flush;
        outFile.close();

        /// Alternatively, outputs can be printed out directly using:
        /// dtw.saveToFile("results.txt", DTW::SAVE_TO_ONE_FILE);

        return 0;
}
